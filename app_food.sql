/* CREATE TABLE */
CREATE TABLE food_type (
type_id INTEGER PRIMARY KEY AUTO_INCREMENT,
type_name VARCHAR(100)
);

CREATE TABLE food (
food_id INTEGER PRIMARY KEY AUTO_INCREMENT,
food_name VARCHAR(100),
image VARCHAR(255),
price FLOAT,
description VARCHAR(255),
type_id INTEGER,
FOREIGN KEY (type_id) REFERENCES food_type(type_id)
);


CREATE TABLE sub_food (
sub_id INTEGER PRIMARY KEY AUTO_INCREMENT,
sub_name VARCHAR(255),
sub_price FLOAT,
food_id INTEGER,
FOREIGN KEY (food_id) REFERENCES food(food_id)
);

CREATE TABLE users (
user_id INTEGER PRIMARY KEY AUTO_INCREMENT,
full_name VARCHAR(255),
email VARCHAR(100),
pass VARCHAR(100)
);

CREATE TABLE user_order (
user_id INTEGER,
food_id INTEGER,
amount INTEGER,
code_order VARCHAR(100),
arr_sub_id VARCHAR(100),
FOREIGN KEY (user_id) REFERENCES users(user_id),
FOREIGN KEY (food_id) REFERENCES food(food_id)
);

CREATE TABLE restaurant (
res_id INTEGER PRIMARY KEY AUTO_INCREMENT,
res_name VARCHAR(255),
image VARCHAR(255),
description VARCHAR(255)
);

CREATE TABLE like_res (
user_id INTEGER,
res_id INTEGER,
date_like DATETIME,
FOREIGN KEY (user_id) REFERENCES users(user_id),
FOREIGN KEY (res_id) REFERENCES restaurant(res_id)
);

CREATE TABLE rate_res (
user_id INTEGER,
res_id INTEGER,
amount INTEGER,
date_rate DATETIME,
FOREIGN KEY (user_id) REFERENCES users(user_id),
FOREIGN KEY (res_id) REFERENCES restaurant(res_id)
);

/* INSERT DATA */
INSERT INTO users (full_name, email, pass) VALUES 
("Nguyễn Văn A", "nguyenvand@gmail.com", "nguyenvand123"),
("Nguyễn Văn B", "nguyenvand@gmail.com", "nguyenvand123"),
("Nguyễn Văn C", "nguyenvand@gmail.com", "nguyenvand123"),
("Nguyễn Văn D", "nguyenvand@gmail.com", "nguyenvand123"),
("Nguyễn Văn e", "nguyenvane@gmail.com", "nguyenvane123"),
("Nguyễn Văn r", "nguyenvanr@gmail.com", "nguyenvanr123"),
("Nguyễn Văn t", "nguyenvant@gmail.com", "nguyenvant123"),
("Nguyễn Văn g", "nguyenvang@gmail.com", "nguyenvang123"),
("Nguyễn Văn h", "nguyenvanh@gmail.com", "nguyenvanh123"),
("Nguyễn Văn j", "nguyenvanj@gmail.com", "nguyenvanj123"),
("Nguyễn Văn u", "nguyenvanu@gmail.com", "nguyenvanu123"),
("Nguyễn Văn i", "nguyenvani@gmail.com", "nguyenvani123"),
("Nguyễn Văn k", "nguyenvank@gmail.com", "nguyenvank123"),
("Nguyễn Văn n", "nguyenvann@gmail.com", "nguyenvann123"),
("Nguyễn Văn q", "nguyenvanq@gmail.com", "nguyenvanq123"),
("Nguyễn Văn v", "nguyenvanv@gmail.com", "nguyenvanv123"),
("Nguyễn Văn p", "nguyenvanp@gmail.com", "nguyenvanp123"),
("Nguyễn Văn o", "nguyenvano@gmail.com", "nguyenvano123"),
("Nguyễn Văn l", "nguyenvanl@gmail.com", "nguyenvanl123"),
("Nguyễn Văn hh", "nguyenvanhh@gmail.com", "nguyenvanhh123"),
("Nguyễn Văn nn", "nguyenvandnn@gmail.com", "nguyenvand1nn3"),
("Nguyễn Văn kkk", "nguyenvandkkk@gmail.com", "nguyenvand123kkk"),
("Nguyễn Văn dfdxc", "nguyenvand@gmail.com", "dfdxcnguyenvand123dfdxc");

INSERT INTO restaurant (res_name, image, description) VALUES
("Restaurant 1", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 1"),
("Restaurant 2", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 2"),
("Restaurant 3", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 3"),
("Restaurant 4", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 4"),
("Restaurant 5", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 5"),
("Restaurant 6", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 6"),
("Restaurant 7", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 7"),
("Restaurant 8", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 8"),
("Restaurant 9", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 9"),
("Restaurant 10", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 10"),
("Restaurant 11", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 11"),
("Restaurant 12", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 12"),
("Restaurant 13", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 13"),
("Restaurant 14", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 14"),
("Restaurant 15", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 15"),
("Restaurant 16", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 16"),
("Restaurant 17", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 17"),
("Restaurant 18", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 18"),
("Restaurant 19", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 19"),
("Restaurant 20", "https://image-tc.galaxy.tf/wijpeg-f0ncqqa8pbenxbptdo1hr5r30/the-restaurant-980x652.jpg", "Restaurant 20");

INSERT INTO rate_res (user_id, res_id, amount) VALUES
(24, 2, 5),
(25, 3, 34),
(60, 7, 6),
(61, 7, 3),
(30, 20, 3),
(34, 20, 3),
(51, 12, 4),
(45, 17, 7),
(37, 13, 7),
(39, 19, 8),
(50, 11, 8);

INSERT INTO food (food_name, price, type_id) VALUES
("Food 1", "1000", 1),
("Food 2", "2000", 2),
("Food 3", "3000", 3),
("Food 4", "4000", 4),
("Food 5", "5000", 5),
("Food 6", "6000", 6),
("Food 7", "7000", 3),

INSERT INTO food_type (type_name) VALUES
("Food type 1"),
("Food type 2"),
("Food type 3"),
("Food type 4"),
("Food type 5"),
("Food type 6");

INSERT INTO user_order (user_id, food_id, amount, code) VALUES
(25, 2, 2, "code 1"),
(34, 5, 2, "code 2"),
(54, 13, 3, "code 3"),
(27, 13, 1, "code 4"),
(32, 3, 5, "code 5"),
(38, 2, 7, "code 6"),
(59, 10, 1, "code 9");

INSERT INTO like_res (user_id, food_id) VALUES
(34, 1),
(44, 2),
(26, 13),
(26, 14),
(34, 2),
(36, 3),
(45, 3),
(45, 12),
(47, 12),
(53, 1);

/* Tìm 5 người đã like nhà hàng nhiều nhất. */
SELECT users.full_name ,count(users.user_id) AS count FROM users 
LEFT JOIN like_res ON users.user_id = like_res.user_id
GROUP BY users.user_id
ORDER BY count(users.user_id) DESC
LIMIT 5;

/* Tìm 2 nhà hàng có lượt like nhiều nhất. */
SELECT restaurant.res_name ,count(restaurant.res_id) AS count FROM restaurant 
LEFT JOIN like_res ON restaurant.res_id = like_res.res_id
GROUP BY restaurant.res_id
ORDER BY count(restaurant.res_id) DESC
LIMIT 2;

/* Tìm người đã đặt hàng nhiều nhất. */
SELECT users.full_name ,count(users.user_id) AS count FROM users 
LEFT JOIN user_order ON users.user_id != user_order.user_id
GROUP BY users.user_id
ORDER BY count(users.user_id) DESC
LIMIT 1;

/* Tìm người dùng không hoạt động trong hệ thống */
SELECT users.user_id ,users.full_name  FROM users
LEFT JOIN user_order ON users.user_id != user_order.user_id
LEFT JOIN like_res ON users.user_id != like_res.user_id
LEFT JOIN rate_res ON users.user_id != rate_res.user_id
GROUP BY users.user_id
